function runGame(startImmediately) {
  var game = new SeeBattle('gameArea');
  game.run();

  if (startImmediately) {
    game.startNewGame();
  }
}
document.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#startGame').addEventListener('click', function() {
    runGame(true);
  })
});
