(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD: import SeeBattle from "sea-battle";
    define(['sea-battle'], factory);
  } else {
    // globals: window.SeeBattle
    root.SeeBattle = factory();
  }
}(typeof self !== 'undefined' ? self : this, function() {

  const createEl = (tagName) => document.createElement(tagName);
  const getElById = (elementId) => document.getElementById(elementId);

  function SeeBattle(gameAreaId) {
    this.gameFieldBorderX = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
    this.gameFieldBorderY = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
    this.gameArea = getElById(gameAreaId);
    this.gameArea.innerHTML = '';
    this.shipsConfiguration = [
      {maxShips: 1, pointCount: 4},
      {maxShips: 2, pointCount: 3},
      {maxShips: 3, pointCount: 2},
      {maxShips: 4, pointCount: 1}
    ];
    this.userName = null;
    this.pcName = null;
    this.pcDelay = 800;

    this._hitsForWin = 0;
    for (let i = 0; i < this.shipsConfiguration.length; i++) {
      this._hitsForWin = +this._hitsForWin + (this.shipsConfiguration[i].maxShips * this.shipsConfiguration[i].pointCount);
    }

    this._pcShipsMap = null;
    this._userShipsMap = null;
    this._gameStopped = false;

    this.CELL_WITH_SHIP = 1;
    this.CELL_EMPTY = 0;

    /**
     * Html элементы
     */
    this.pcInfo = null;
    this.userInfo = null;
    this.toolbar = null;
    this.startGameButton = null;
    this.pcGameField = null;
    this.userGameField = null;
  }

  SeeBattle.prototype = {

    run: function() {
      this.createToolbar();
      this.createGameFields();
      this.createFooter();
    },
    createToolbar: function() {
      this.toolbar = createEl('div');
      this.toolbar.setAttribute('class', 'toolbar');
      this.gameArea.appendChild(this.toolbar);
    },
    createGameFields: function() {
      const pcGameArea = createEl('div');
      pcGameArea.setAttribute('class', 'pcGameArea');
      this.gameArea.appendChild(pcGameArea);

      const userGameArea = createEl('div');
      userGameArea.setAttribute('class', 'userGameArea');
      this.gameArea.appendChild(userGameArea);

      this.pcInfo = createEl('div');
      this.pcInfo.setAttribute('class', 'tx-center');
      pcGameArea.appendChild(this.pcInfo);

      this.userInfo = createEl('div');
      userGameArea.appendChild(this.userInfo);

      this.pcGameField = createEl('div');
      this.pcGameField.setAttribute('class', 'gameField');
      this.userGameField = createEl('div');
      this.userGameField.setAttribute('class', 'gameField');
      pcGameArea.appendChild(this.pcGameField);
      userGameArea.appendChild(this.userGameField);
    },
    createFooter: function() {
      const footer = createEl('div');
      footer.setAttribute('class', 'footer');

      this.startGameButton = createEl('button');
      this.startGameButton.innerHTML = 'Почати гру';
      this.startGameButton.setAttribute('class', 'btn');
      this.startGameButton.onclick = function() {
        this.startNewGame();
      }.bind(this);
      footer.appendChild(this.startGameButton);

      this.gameArea.appendChild(footer);
    },
    startNewGame: function() {
      this.userName = this.userName || prompt('Ваше ім\'я?', 'ЗСУ');
      this.pcName = this.pcName || prompt('Ім\'я супротивника', 'Оркостан');

      if (!this.userName || !this.pcName) {
        alert('Неверно указали имя');
        return;
      }

      this.startGameButton.innerHTML = 'Почати спочатку...';
      this.pcInfo.innerHTML = this.pcName + ' (ваш противник)';
      this.userInfo.innerHTML = this.userName + ' (ваше поле)';

      this._pcShipsMap = this.generateRandomShipMap();
      this._userShipsMap = this.generateRandomShipMap();
      this._pcShotMap = this.generateShotMap();
      this._userHits = 0;
      this._pcHits = 0;
      this._blockHeight = null;
      this._gameStopped = false;
      this._pcGoing = false;

      this.drawGamePoints();
      this.updateToolbar();
    },

    drawGamePoints: function() {
      for (let yPoint = 0; yPoint < this.gameFieldBorderY.length; yPoint++) {
        for (let xPoint = 0; xPoint < this.gameFieldBorderX.length; xPoint++) {
          const pcPointBlock = this.getOrCreatePointBlock(yPoint, xPoint);
          pcPointBlock.onclick = function(e) {
            this.userFire(e);
          }.bind(this);
          // если нужно отобразить корабли компбютера
          /*if(this._pcShipsMap[yPoint][xPoint] === this.CELL_WITH_SHIP){
              pcPointBlock.setAttribute('class', 'ship');
          }*/

          const userPointBlock = this.getOrCreatePointBlock(yPoint, xPoint, 'user');
          if (this._userShipsMap[yPoint][xPoint] === this.CELL_WITH_SHIP) {
            userPointBlock.setAttribute('class', 'ship');
          }
        }
      }
    },

    _blockHeight: null,

    getOrCreatePointBlock: function(yPoint, xPoint, type) {
      const id = this.getPointBlockIdByCoords(yPoint, xPoint, type);
      let block = getElById(id);
      if (block) {
        block.innerHTML = '';
        block.setAttribute('class', '');
      } else {
        block = createEl('div');
        block.setAttribute('id', id);
        block.setAttribute('data-x', xPoint);
        block.setAttribute('data-y', yPoint);
        if (type && type === 'user') {
          this.userGameField.appendChild(block);
        } else {
          this.pcGameField.appendChild(block);
        }
      }
      block.style.width = (100 / this.gameFieldBorderY.length) + '%';
      if (!this._blockHeight) {
        this._blockHeight = block.clientWidth;
      }
      block.style.height = this._blockHeight + 'px';
      block.style.lineHeight = this._blockHeight + 'px';
      block.style.fontSize = this._blockHeight + 'px';
      return block;
    },

    getPointBlockIdByCoords: function(yPoint, xPoint, type) {
      if (type && type === 'user') {
        return 'user_x' + xPoint + '_y' + yPoint;
      }
      return 'pc_x' + xPoint + '_y' + yPoint;
    },

    generateShotMap: function() {
      const map = [];
      for (let yPoint = 0; yPoint < this.gameFieldBorderY.length; yPoint++) {
        for (let xPoint = 0; xPoint < this.gameFieldBorderX.length; xPoint++) {
          map.push({y: yPoint, x: xPoint});
        }
      }
      return map;
    },

    generateRandomShipMap: function() {
      let i;
      const map = [];

      for (let yPoint = -1; yPoint < (this.gameFieldBorderY.length + 1); yPoint++) {
        for (let xPoint = -1; xPoint < (this.gameFieldBorderX.length + 1); xPoint++) {
          if (!map[yPoint]) {
            map[yPoint] = [];
          }
          map[yPoint][xPoint] = this.CELL_EMPTY;
        }
      }

      let shipsConfiguration = JSON.parse(JSON.stringify(this.shipsConfiguration));
      let allShipsPlaced = false;
      while (allShipsPlaced === false) {
        let xPoint = this.getRandomInt(0, this.gameFieldBorderX.length);
        let yPoint = this.getRandomInt(0, this.gameFieldBorderY.length);
        if (this.isPointFree(map, xPoint, yPoint) === true) {
          if (this.canPutHorizontal(map, xPoint, yPoint, shipsConfiguration[0].pointCount, this.gameFieldBorderX.length)) {
            for (i = 0; i < shipsConfiguration[0].pointCount; i++) {
              map[yPoint][xPoint + i] = this.CELL_WITH_SHIP;
            }
          } else if (this.canPutVertical(map, xPoint, yPoint, shipsConfiguration[0].pointCount, this.gameFieldBorderY.length)) {
            for (i = 0; i < shipsConfiguration[0].pointCount; i++) {
              map[yPoint + i][xPoint] = this.CELL_WITH_SHIP;
            }
          } else {
            continue;
          }

          shipsConfiguration[0].maxShips--;
          if (shipsConfiguration[0].maxShips < 1) {
            shipsConfiguration.splice(0, 1);
          }
          if (shipsConfiguration.length === 0) {
            allShipsPlaced = true;
          }
        }
      }
      return map;
    },
    getRandomInt: function(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    },

    isPointFree: function(map, xPoint, yPoint) {
      return map[yPoint][xPoint] === this.CELL_EMPTY
        && map[yPoint - 1][xPoint] === this.CELL_EMPTY
        && map[yPoint - 1][xPoint + 1] === this.CELL_EMPTY
        && map[yPoint][xPoint + 1] === this.CELL_EMPTY
        && map[yPoint + 1][xPoint + 1] === this.CELL_EMPTY
        && map[yPoint + 1][xPoint] === this.CELL_EMPTY
        && map[yPoint + 1][xPoint - 1] === this.CELL_EMPTY
        && map[yPoint][xPoint - 1] === this.CELL_EMPTY
        && map[yPoint - 1][xPoint - 1] === this.CELL_EMPTY;

    },

    canPutHorizontal: function(map, xPoint, yPoint, shipLength, coordLength) {
      let freePoints = 0;
      for (let x = xPoint; x < coordLength; x++) {

        if (map[yPoint][x] === this.CELL_EMPTY
          && map[yPoint - 1][x] === this.CELL_EMPTY
          && map[yPoint - 1][x + 1] === this.CELL_EMPTY
          && map[yPoint][x + 1] === this.CELL_EMPTY
          && map[yPoint + 1][x + 1] === this.CELL_EMPTY
          && map[yPoint + 1][x] === this.CELL_EMPTY
        ) {
          freePoints++;
        } else {
          break;
        }
      }
      return freePoints >= shipLength;
    },

    canPutVertical: function(map, xPoint, yPoint, shipLength, cordLength) {
      let freePoints = 0;
      for (let y = yPoint; y < cordLength; y++) {
        if (map[y][xPoint] === this.CELL_EMPTY
          && map[y + 1][xPoint] === this.CELL_EMPTY
          && map[y + 1][xPoint + 1] === this.CELL_EMPTY
          && map[y + 1][xPoint] === this.CELL_EMPTY
          && map[y][xPoint - 1] === this.CELL_EMPTY
          && map[y - 1][xPoint - 1] === this.CELL_EMPTY
        ) {
          freePoints++;
        } else {
          break;
        }
      }
      return freePoints >= shipLength;
    },

    userFire: function(event) {
      if (this.isGameStopped() || this.isPCGoing()) {
        return;
      }
      const e = event || window.event;
      const firedEl = e.target || e.srcElement;
      const {x, y} = firedEl.dataset;
      if (this._pcShipsMap[y][x] === this.CELL_EMPTY) {
        firedEl.innerHTML = this.getFireFailTemplate();
        this.prepareToPcFire();
      } else {
        firedEl.innerHTML = this.getFireSuccessTemplate();
        firedEl.setAttribute('class', 'ship');
        this._userHits++;
        this.updateToolbar();
        if (this._userHits >= this._hitsForWin) {
          this.stopGame();
        }
      }
      firedEl.onclick = null;
    },
    _pcGoing: false,
    isPCGoing: function() {
      return this._pcGoing;
    },
    prepareToPcFire: function() {
      this._pcGoing = true;
      this.updateToolbar();
      setTimeout(function() {
        this.pcFire();
      }.bind(this), this.pcDelay);
    },
    pcFire: function() {
      if (this.isGameStopped()) {
        return;
      }
      // берется случайный выстрел из сгенерированной ранее карты
      const randomShotIndex = this.getRandomInt(0, this._pcShotMap.length);
      const randomShot = JSON.parse(JSON.stringify(this._pcShotMap[randomShotIndex]));
      // удаление чтобы не было выстрелов повторных
      this._pcShotMap.splice(randomShotIndex, 1);

      const firedEl = getElById(this.getPointBlockIdByCoords(randomShot.y, randomShot.x, 'user'));
      if (this._userShipsMap[randomShot.y][randomShot.x] === this.CELL_EMPTY) {
        firedEl.innerHTML = this.getFireFailTemplate();
      } else {
        firedEl.innerHTML = this.getFireSuccessTemplate();
        this._pcHits++;
        this.updateToolbar();
        if (this._pcHits >= this._hitsForWin) {
          this.stopGame();
        } else {
          this.prepareToPcFire();
        }
      }
      this._pcGoing = false;
      this.updateToolbar();
    },
    stopGame: function() {
      this._gameStopped = true;
      this._pcGoing = false;
      this.startGameButton.innerHTML = 'Зіграти ще раз?';
      this.updateToolbar();
    },
    isGameStopped: function() {
      return this._gameStopped;
    },
    getFireSuccessTemplate: function() {
      return '💣';
    },
    getFireFailTemplate: function() {
      return '📍';
    },

    /**
     * Отображение текущей игровой ситуации в блоке
     */
    updateToolbar: function() {
      this.toolbar.innerHTML = 'Рахунок - ' + this._pcHits + ':' + this._userHits;
      if (this.isGameStopped()) {
        if (this._userHits >= this._hitsForWin) {
          this.toolbar.innerHTML += ', ви виграли';
        } else {
          this.toolbar.innerHTML += ', переміг ваш супротивник';
        }
      } else if (this.isPCGoing()) {
        this.toolbar.innerHTML += ', ходить ваш супротивник';
      } else {
        this.toolbar.innerHTML += ', зараз ваш хід';
      }
    }
  };

  return SeeBattle;
}));
